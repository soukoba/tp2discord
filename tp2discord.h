#ifndef _TP2D_
#define _TP2D_

#define DEBUG
struct tweet_data
{
	time_t time_data;
	uint8_t is_photo;
	char * media_key;
	/*
		Use expanded_url
	*/
	char * url;
};

void printf_debug(const char * format, ...);

#endif