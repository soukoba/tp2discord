#ifndef _EXTRACT_DATA_
#define _EXTRACT_DATA_

int get_media_key(cJSON * tweet, struct tweet_data * td);
int get_url(cJSON * tweet, struct tweet_data * td);
int get_timedata(cJSON * tweet, struct tweet_data * td);
int get_type(cJSON * twitter_data, struct tweet_data *td);
#endif