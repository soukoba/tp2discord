
CFLAGS = \
	-W \
	-Wall \
	-Wextra \
	-Wparentheses \
	-Wmissing-braces \
	-pedantic \

INCLUDES = \
	-I .\

objects= \
	tp2discord.o \
	cJSON.o \
	time_parser.o \
	extract_data.o \

all: $(objects)
	$(CC) -o tp2d $(objects) -lcurl

$(objects): %.o: %.c
	@$(CC) -c -g $(CFLAGS) $(INCLUDES) $< -o $@

clean:
	@rm -f tp2d
	@rm -f *.o
