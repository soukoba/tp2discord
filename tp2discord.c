#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <stdarg.h>
#include <string.h>
#include <stddef.h>
#include <unistd.h>
#include <time.h>
#include <curl/curl.h>
#include "tp2discord.h"
#include "cJSON.h"
#include "time_parser.h"
#include "extract_data.h"

static char *myname;

FILE * dbo;

static char json_buffer[65536];
static uint16_t json_size;

void usage()
{
	printf("%s file.json\n", myname);
}

void printf_debug(const char * format, ...)
{
#ifdef DEBUG
	char buff[1024];
	va_list args;
	va_start(args, format);
	vsprintf(buff, format, args);
	fprintf(dbo, "%s", buff);
	va_end(args);
#else
	(void)format;
#endif
}

static void get_data(char *ptr, size_t size, size_t nmemb, void *userdata)
{
	int i;
	(void)size;
	(void)userdata;

	memset(json_buffer, 0, sizeof(json_buffer));
	json_size = nmemb;
	for (i = 0; i < json_size; i++)
	{
		json_buffer[i] = ptr[i];
	}
}

static int get_web_info(const char * web_info_path, char * bearer, 
			char * turl, char * durl)
{
	size_t size;
	char json_buffer[4096];
	FILE * web_info_file;
	cJSON * web_info;
	cJSON * jdata;


	if ((web_info_file = fopen(web_info_path, "r")) == 0)
	{
		printf("Couldn't open web info file!\n");
		return -1;
	}

	size = fread(json_buffer, sizeof(char), 4095, web_info_file);
	
	if (size)
	{
		json_buffer[size++] = '\0';
	}
	else
	{
		return -2;
	}

	web_info = cJSON_ParseWithLength(json_buffer, size);

	jdata = cJSON_GetObjectItemCaseSensitive(web_info, 
			"twitter_bearer_token");
	if (jdata)
	{
		strcpy(bearer, jdata->valuestring);
		printf_debug("Twitter Bearer: %s\n", jdata->valuestring);	
	}
	else
	{
		printf("Couldn't find twitter_bearer_token");
		return -3;
	}

	jdata = cJSON_GetObjectItemCaseSensitive(web_info, 
			"twitter_url");
	if (jdata)
	{
		strcpy(turl, jdata->valuestring);
		printf_debug("Twitter URL: %s\n", jdata->valuestring);
	}
	else
	{
		printf("Couldn't find twitter_url");
		return -4;
	}
	
	jdata = cJSON_GetObjectItemCaseSensitive(web_info, 
			"discord_webhook");
	if (jdata)
	{
		strcpy(durl, jdata->valuestring);
		printf_debug("Discord Webhook: %s\n", jdata->valuestring);
	}
	else
	{
		printf("Couldn't find discord_webhook");
		return -5;
	}

	printf_debug("------------------------------------------------\n");

	return 0;
}
/*
	Start of main
	Notes:
		Need to save externally:
			time_data
*/
int main(int argc, char *argv[])
{
	int rv;
	int i;
	FILE * saved_time_file;
	time_t saved_time_data;
	char saved_time_string[64];
	
	cJSON *twitter_data;
	cJSON *jdata;
	cJSON *tweet;
	char json_packet[8196];

	struct tweet_data tdarray[5];
	CURL * curl;
	struct curl_slist *curl_list;
	uint8_t posted;

	char twitter_bearer[256];
	char twitter_url[256];
	char discord_url[256];

	memset(twitter_bearer, 0, sizeof(twitter_bearer));
	memset(twitter_url, 0, sizeof(twitter_url));
	memset(discord_url, 0, sizeof(discord_url));

	myname = argv[0];

	if (argc < 2)
	{
		usage();
		return -1;
	}
	curl_list = 0;
	/*
		Open up a debug file
	*/
#ifdef DEBUG
	if ((dbo = fopen("debug_out", "a+")) == 0)
	{
		printf("Couldn't open debug file!\n");
		return -1;
	}
#endif

	/*
		Get web info
	*/
	rv = get_web_info(argv[1], twitter_bearer, twitter_url, discord_url);
	if (rv < 0)
	{
		printf("Couldn't get web info!\n");
		return -1;
	}

	/*
		Get twitter data
	*/

	curl = curl_easy_init();

	curl_easy_setopt(curl, CURLOPT_URL, twitter_url);
	curl_easy_setopt(curl, CURLOPT_XOAUTH2_BEARER, twitter_bearer);
	curl_easy_setopt(curl, CURLOPT_HTTPAUTH, CURLAUTH_BEARER);
	curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, get_data);

	rv = curl_easy_perform(curl);

	twitter_data = cJSON_ParseWithLength(json_buffer, json_size);
	jdata = cJSON_GetObjectItemCaseSensitive(twitter_data, "data");

	/*
		Parse through twitter data
	*/
	i = 0;
	if (!jdata)
	{
		printf("Couldn't find any twitter data!");
		return -1;
	}
	
	cJSON_ArrayForEach(tweet, jdata)
	{
		/*
			Get media key
			Note:
				Only getting first key
		*/
		rv = get_media_key(tweet, &tdarray[i]);
		printf_debug("get_key: %s (%d)\n", (rv ? "FAIL" : "PASS"), rv);

		/*
			Get URL
			Note:
				Getting expanded
		*/
		if (!rv)
		{
			rv = get_url(tweet, &tdarray[i]);
			printf_debug("get_url: %s (%d)\n", (rv ? "FAIL" : "PASS"), rv);

			/*
				Get Timedata
			*/
			rv = get_timedata(tweet, &tdarray[i]);
			printf_debug("get_timedata: %s (%d)\n", (rv ? "FAIL" : "PASS"), rv);

			/*
				Get type
			*/
			rv = get_type(twitter_data, &tdarray[i]);
			printf_debug("get_type: %s (%d)\n", (rv ? "FAIL" : "PASS"), rv);
		}
		else
		{
			tdarray[i].is_photo = 0;
		}

#ifdef DEBUG

		printf_debug("\nTweet %d:\n", i);
		if (tdarray[i].time_data)
		{	
			printf_debug("epoch: %ld\n", tdarray[i].time_data);
		}	
		if (tdarray[i].url)
		{	
			printf_debug("url: %s\n", tdarray[i].url);
		}
		if (tdarray[i].media_key)
		{	
			printf_debug("media_key: %s\n", tdarray[i].media_key);
		}
		printf_debug("is_photo: %d\n\n", tdarray[i].is_photo);
		printf_debug("------------------------------------------------\n");
#endif
		i++;
	}

	/*
		Setup post to discord
	*/
	curl_easy_cleanup(curl);
	curl = curl_easy_init();
	curl_list = curl_slist_append(curl_list, "Content-Type: application/json");

	curl_easy_setopt(curl, CURLOPT_URL, discord_url);
	curl_easy_setopt(curl, CURLOPT_HTTPHEADER, curl_list);

	saved_time_file = fopen("latest_time", "r+");
	if (!saved_time_file)
	{
		/*
			File doesn't exist. Generate
		*/
		printf("Couldn't find saved_time file!\n");
		printf("Generating...\n");
		saved_time_file = fopen("latest_time", "w+");
		saved_time_data = 0;
		fseek(saved_time_file, 0, SEEK_SET);
		fprintf(saved_time_file, "%032ld\n", saved_time_data);
	}
	else
	{
		/*
			Get time from file
		*/
		memset(saved_time_string, 0, sizeof(saved_time_string));
		fgets(saved_time_string, sizeof(saved_time_string), saved_time_file);
		saved_time_data = strtol(saved_time_string, 0, 10);
	}
	
	/*
		iterate through tdarray from the top
	*/
	posted = 0;
	for (i = sizeof(tdarray)/sizeof(struct tweet_data); i > 0; i--)
	{
		if ((	saved_time_data < tdarray[i - 1].time_data) && 
				tdarray[i - 1].is_photo)
		{
			/*
				save time to file
			*/
			saved_time_data = tdarray[i - 1].time_data;
			fseek(saved_time_file, 0, SEEK_SET);
			fprintf(saved_time_file, "%032ld\n", saved_time_data);
			
			/*
				Construct json_packet
			*/
			memset(json_packet, 0, sizeof(json_packet));
			strcat(json_packet, "{");
			strcat(json_packet, "\"content\": \"");
			strcat(json_packet, tdarray[i - 1].url);
			strcat(json_packet, "\"");
			strcat(json_packet, "}");

			curl_easy_setopt(curl, CURLOPT_POSTFIELDS, json_packet);
			
			/*
				Post to discord
			*/
			rv = curl_easy_perform(curl);
			if (!rv)
			{
				posted = 1;
			}
			else
			{
				printf("Something failed to post!\n");
			}
		}
	}

	fclose(saved_time_file);	
	curl_easy_cleanup(curl);

	return posted ? 0 : 0xff;
}
