#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <time.h>
#include <string.h>
#include "tp2discord.h"
#include "time_parser.h"

uint8_t tssm_state;
uint8_t tssm_error;

static int is_del(char in)
{
	if (in == '-' || in == 'T' || in == ':' || in == '.' || in == 'Z')
	{
		return 1;
	}

	return 0;
}

static int is_num(char in)
{
	if (in >= '0' || in <= '9')
	{
		return 1;
	}

	return 0;
}

/*
	Start of ISO 8601 parser
*/

void reset_tssm()
{
	tssm_state = STATE_YEAR;
	tssm_error = 0;
}

static void year_in(char in, struct tm * time_data)
{
	if (is_del(in))
	{
		time_data->tm_year -= 1900;
#ifdef DEBUG
		printf_debug("years since 1900: %d\n", time_data->tm_year);
		printf_debug("year: %d\n", (time_data->tm_year + 1900));
#endif
		tssm_state = STATE_MONTH;
	}
	else if(is_num(in))
	{
		time_data->tm_year *= 10;
		time_data->tm_year += in - '0';
	}
	else
	{
		tssm_error = 1;
		printf("Error parsing year\n");
		tssm_state = STATE_DONE;
	}
	
}

static void month_in(char in, struct tm * time_data)
{
	if (is_del(in))
	{
		time_data->tm_mon -= 1;
#ifdef DEBUG
		printf_debug("month:%d\n", time_data->tm_mon);
#endif
		tssm_state = STATE_DAY;
	}
	else if(is_num(in))
	{
		time_data->tm_mon *= 10;
		time_data->tm_mon += in - '0';
	}
	else
	{
		tssm_error = 1;
		printf("Error parsing month\n");
		tssm_state = STATE_DONE;
	}
	
}

static void day_in(char in, struct tm * time_data)
{
	if (is_del(in))
	{
#ifdef DEBUG
		printf_debug("day:%d\n", time_data->tm_mday);
#endif
		tssm_state = STATE_HOUR;
	}
	else if(is_num(in))
	{
		time_data->tm_mday *= 10;
		time_data->tm_mday += in - '0';
	}
	else
	{
		tssm_state = STATE_DONE;
		tssm_error = 1;
		printf("Error parsing day\n");
	}
	
}

static void hour_in(char in, struct tm * time_data)
{
	if (is_del(in))
	{
#ifdef DEBUG
		printf_debug("hour:%d\n", time_data->tm_hour);
#endif
		tssm_state = STATE_MINUTE;
	}
	else if(is_num(in))
	{
		time_data->tm_hour *= 10;
		time_data->tm_hour += in - '0';
	}
	else
	{
		tssm_state = STATE_DONE;
		tssm_error = 1;
		printf("Error parsing minute\n");
	}
}

static void minute_in(char in, struct tm * time_data)
{
	if (is_del(in))
	{
#ifdef DEBUG
		printf_debug("minute:%d\n", time_data->tm_min);
#endif
		tssm_state = STATE_SECOND;
	}
	else if(is_num(in))
	{
		time_data->tm_min *= 10;
		time_data->tm_min += in - '0';
	}
	else
	{
		tssm_state = STATE_DONE;
		tssm_error = 1;
		printf("Error parsing minute\n");
	}
}

static void second_in(char in, struct tm * time_data)
{
	if (is_del(in))
	{
#ifdef DEBUG
		printf_debug("second:%d\n", time_data->tm_sec);
#endif
		tssm_state = STATE_MS;
	}
	else if(is_num(in))
	{
		time_data->tm_sec *= 10;
		time_data->tm_sec += in - '0';
	}
	else
	{
		tssm_state = STATE_DONE;
		tssm_error = 1;
		printf("Error parsing second\n");
	}	
}

static void ms_in(char in, struct tm * time_data)
{
	(void)time_data;
	if (is_del(in))
	{
		tssm_state = STATE_DONE;
	}
	else if(is_num(in))
	{
		/*
			Do nothing for now?
		*/
	}
	else
	{
		tssm_state = STATE_DONE;
		tssm_error = 1;
		printf("Error parsing ms\n");
	}	
}

static void done_in(char in, struct tm * time_data)
{
	(void)in;
	(void)time_data;
}

void (*tssm[8])(char, struct tm * time_data) =
{
	year_in,
	month_in,
	day_in,
	hour_in,
	minute_in,
	second_in,
	ms_in,
	done_in
};